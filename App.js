import React from 'react';
import { useFonts } from '@use-expo/font';
import { connect, Provider } from 'react-redux';
import { compose } from 'redux';
import { PersistGate } from 'redux-persist/integration/react';
import { AppLoading } from 'expo';
import { createStructuredSelector } from 'reselect';
import Main from 'containers/Main';
import { store, persistor } from 'src/store';
// import {
// } from '@expo-google-fonts/open-sans';

const RobotoFontM = require('fonts/Roboto-Medium.ttf');
const Helvetica = require('fonts/HelveticaNeue.ttf');
const HelveticaBold = require('fonts/HelveticaNeueBold.ttf');
const Segoeui = require('fonts/SegoeUI.ttf');
const SegoeuiBold = require('fonts/SegoeUIBold.ttf');

function AppWrapper(props) {
    const { authToken } = props;
    let content = null;
    // if (typeof authToken === 'string' || authToken == null) {
    if (true) { // Todo change after authentication setup
        content = (
            <>
                <Main authToken={authToken} />
            </>
        );
    }
    return (content);
}

const mapStateToProps = createStructuredSelector({
    // authToken: selectAuthToken(),
});

const withConnect = connect(mapStateToProps, null);

export const Wrapper = compose(withConnect)(AppWrapper);

export default function App() {
    const [fontsLoaded] = useFonts({
        Roboto_medium: RobotoFontM,
        Helvetica,
        Helvetica_bold: HelveticaBold,
        Segoeui,
        SegoeuiBold,
    });

    if (!fontsLoaded) {
        return <AppLoading />;
    }
    return (
        <Provider store={store}>
            <PersistGate loading={null} persistor={persistor}>
                <Wrapper />
            </PersistGate>
        </Provider>
    );
}
