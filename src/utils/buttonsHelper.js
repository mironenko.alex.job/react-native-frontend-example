import React from 'react';
import { TouchableOpacity, Text } from 'react-native';

export const buttonBuild = (action, text, textStyles = {}, containerStyles = {}) => (
    <TouchableOpacity
        style={[containerStyles]}
        onPress={() => {
            if (action) {
                action();
            }
        }}
    >
        <Text style={[textStyles]}>{text}</Text>
    </TouchableOpacity>
);
