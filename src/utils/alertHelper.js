import { Alert } from 'react-native';

export const openInDevelopmentAlert = () => Alert.alert(
    'Ooops..',
    'Element is under development',
    [
        { text: 'OK' },
    ],
    { cancelable: false },
);
