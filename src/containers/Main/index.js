import React from 'react';
import PropTypes from 'prop-types';
import { AppLoading } from 'expo';

import Navigator from 'containers/Navigator';
// import FooterEl from 'components/Footer';

function Main(props) {
    const { authToken } = props;
    return (
        <>
            <Navigator authToken={authToken} />
            {/*{authToken && authToken.length > 0 ? <FooterEl /> : null }*/}
        </>
    );
}

Main.propTypes = {
    authToken: PropTypes.string,
};

export default Main;
