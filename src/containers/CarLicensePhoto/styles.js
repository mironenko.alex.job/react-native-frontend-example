import { StyleSheet, Dimensions } from 'react-native';

const { width } = Dimensions.get('window');

const styles = StyleSheet.create({
    carModel: {
        fontFamily: 'SegoeuiBold',
        fontSize: 20,
        lineHeight: 27,
        textAlign: 'center',
        paddingBottom: 10,
    },
    card: {
        marginTop: 20,
        paddingTop: 32,
        paddingLeft: 15,
        paddingRight: 15,
        paddingBottom: 24,
        backgroundColor: '#fff',
    },
    title: {
        fontFamily: 'Segoeui',
        fontWeight: '600',
        fontSize: 18,
        lineHeight: 24,
        textAlign: 'center',
        color: '#282C38',
    },
    carImgBox: {
        marginTop: 30,
        alignItems: 'center',
    },
    carImgWrap: {
        width: 250,
        borderRadius: 15,
        backgroundColor: 'transparent',
    },
    carImg: {
        width: 250,
        height: 142,
        borderRadius: 15,
        backgroundColor: 'red',
    },
    buttonBox: {
        marginTop: 24,
        alignItems: 'center',
    },
    buttonWrap: {
        width: 250,
    },
});

export default styles;
