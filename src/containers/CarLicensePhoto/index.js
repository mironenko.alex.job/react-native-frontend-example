import React, { useState } from 'react';
import PropTypes from 'prop-types';
import {
    Alert, Image, Text, View,
} from 'react-native';
import * as ImagePicker from 'expo-image-picker';
import Constants from 'expo-constants';
import * as Permissions from 'expo-permissions';

import CarSplash from 'img/carLicenseSplash.png';
import TaxiButton from 'components/TaxiButton';
import UploadPhoto from 'src/containers/UploadPhoto/index';
import s from './styles';

const CarLicensePhoto = (props) => {
    const [uri, setUri] = useState(props.uri);

    const pickImage = async () => {
        try {
            const result = await ImagePicker.launchImageLibraryAsync({
                mediaTypes: ImagePicker.MediaTypeOptions.All,
                allowsEditing: true,
                aspect: [4, 3],
                quality: 1,
            });
            if (!result.cancelled) {
                setUri(result.uri);
                props.setImg(result.uri);
            }
        } catch (E) {
            console.log(E);
        }
    };

    const getPermissionAsync = async () => {
        if (Constants.platform.ios) {
            const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
            if (status !== 'granted') {
                Alert.alert('Sorry, we need camera roll permissions to make this work!');
            } else {
                await pickImage();
            }
        } else {
            await pickImage();
        }
    };
    const openGallery = async () => {
        await getPermissionAsync();
    };

    const { title, handler, showSplash = false } = props;

    let imageComponent = null;
    let btnPhotoText = 'Добавить фото';
    if (uri) {
        btnPhotoText = 'Изменить фото';
        imageComponent = (
            <View style={s.carImgBox}>
                <View style={s.carImgWrap}>
                    <Image
                        source={{ uri }}
                        width={250}
                        height={142}
                        style={[s.carImg]}
                    />
                </View>
            </View>
        );
    } else if (showSplash && !uri) {
        imageComponent = (
            <View style={s.carImgBox}>
                <View style={s.carImgWrap}>
                    <Image source={CarSplash} style={s.carImg} />
                </View>
            </View>
        );
    }
    return (
        <View style={s.card}>
            <Text style={s.title}>{title}</Text>
            {imageComponent}
            <View style={s.buttonBox}>
                <View style={s.buttonWrap}>
                    <TaxiButton
                        title={btnPhotoText}
                        elColor="#FECD0E"
                        titleColor="#FECD0E"
                        height={53}
                        handler={() => openGallery()}
                        isFilled={false}
                    />
                </View>
            </View>
        </View>
    );
};

CarLicensePhoto.propTypes = {
    handler: PropTypes.func,
    title: PropTypes.string,
    showSplash: PropTypes.bool,
};

CarLicensePhoto.propTypes = {
    setImg: PropTypes.func,
    uri: PropTypes.string,
};

export default CarLicensePhoto;
