import React from 'react';
import { Image, Text, TouchableOpacity } from 'react-native';

import DrawerImg from 'img/drawer.png';

const DrawerBtn = (props) => {
    const { navigation } = props;
    return (
        <TouchableOpacity
            style={{width: 60, marginLeft: 10 }}
            onPress={() => navigation.openDrawer()}
        >
            <Image source={DrawerImg} />
        </TouchableOpacity>
    );
};
export default DrawerBtn;
