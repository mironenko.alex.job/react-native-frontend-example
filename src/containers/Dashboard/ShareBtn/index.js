import React from 'react';
import { Image, Text, TouchableOpacity } from 'react-native';
import { openInDevelopmentAlert } from 'utils/alertHelper';

import ShareImg from 'img/share.png';

const ShareBtn = (props) => {
    const { navigation } = props;
    return (
        <TouchableOpacity
            style={{ width: 60, marginRight: 10 }}
            onPress={() => openInDevelopmentAlert()}
        >
            <Image source={ShareImg} />
        </TouchableOpacity>
    );
};
export default ShareBtn;
