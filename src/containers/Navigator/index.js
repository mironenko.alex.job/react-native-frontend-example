import React, { memo } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { NavigationContainer } from '@react-navigation/native';

import { setRoute } from 'global/actions';
import { navigationRef } from 'utils/navigationHelper';
import Routes from 'src/routes';

function Navigator(props) {
    const { authToken } = props;
    const updateRouteState = (route) => {
        const { onSetRoute } = props;
        onSetRoute(route);
    };
    return (
        <NavigationContainer
            ref={navigationRef}
            onStateChange={(route) => {
                updateRouteState(route);
            }}
        >
            <Routes authToken={authToken} />
        </NavigationContainer>
    );
}

Navigator.propTypes = {
    onSetRoute: PropTypes.func.isRequired,
    authToken: PropTypes.string,
};

export function mapDispatchToProps(dispatch) {
    return {
        onSetRoute: (route) => dispatch(setRoute(route)),
    };
}

const withConnect = connect(null, mapDispatchToProps);

export default compose(
    withConnect,
    memo,
)(Navigator);
