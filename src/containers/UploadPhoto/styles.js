import { StyleSheet, Dimensions } from 'react-native';

const { width } = Dimensions.get('window');

const styles = StyleSheet.create({
    wrap: {
        width: '100%',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        backgroundColor: '#fff',

    },
    imgBlock: {
        width: 85,
        height: 85,
        borderRadius: 85,
        backgroundColor: '#F4F3F0',
        marginBottom: 32,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        borderColor: '#FFD328',
        borderWidth: 1,
        marginTop: 36,
    },
    img: {
        width: 85,
        height: 85,
        borderRadius: 85,
    },
    carModel: {
        fontFamily: 'SegoeuiBold',
        fontSize: 20,
        lineHeight: 27,
        textAlign: 'center',
        paddingBottom: 10,
    },
    card: {
        marginTop: 20,
        paddingTop: 32,
        paddingLeft: 15,
        paddingRight: 15,
        paddingBottom: 24,
        backgroundColor: '#fff',
    },
    title: {
        fontFamily: 'Segoeui',
        fontWeight: '600',
        fontSize: 18,
        lineHeight: 24,
        textAlign: 'center',
        color: '#282C38',
    },
    buttonBox: {
        alignItems: 'center',
    },
    buttonWrap: {
        width: 150,
    },
});

export default styles;
