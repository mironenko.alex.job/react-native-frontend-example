import React, { useState } from 'react';
import PropTypes from 'prop-types';
import {
    Alert, Image, View,
} from 'react-native';
import * as ImagePicker from 'expo-image-picker';
import Constants from 'expo-constants';
import * as Permissions from 'expo-permissions';

import TaxiButton from 'components/TaxiButton';
import PhotoIcon from 'img/photoIcon.png';
import s from './styles';

const UploadPhoto = (props) => {
    const [uri, setUri] = useState(null);

    const pickImage = async () => {
        try {
            const result = await ImagePicker.launchImageLibraryAsync({
                mediaTypes: ImagePicker.MediaTypeOptions.All,
                allowsEditing: true,
                aspect: [4, 3],
                quality: 1,
            });
            if (!result.cancelled) {
                setUri(result.uri);
                props.setImg(result.uri);
            }
        } catch (E) {
            console.log(E);
        }
    };

    const getPermissionAsync = async () => {
        if (Constants.platform.ios) {
            const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
            if (status !== 'granted') {
                Alert.alert('Sorry, we need camera roll permissions to make this work!');
            } else {
                await pickImage();
            }
        } else {
            await pickImage();
        }
    };
    const openGallery = async () => {
        await getPermissionAsync();
    };

    let imageComponent = null;
    let btnPhotoText = 'Добавить фото';
    if (uri) {
        btnPhotoText = 'Изменить фото';
        imageComponent = (
            <View style={[s.wrap]}>
                <View style={s.imgBlock}>
                    <Image source={{ uri }} style={s.img} width={250} height={142} />
                </View>
            </View>
        );
    } else if (!uri) {
        imageComponent = (
            <View style={[s.wrap]}>
                <View style={s.imgBlock}>
                    <Image  source={PhotoIcon} />
                </View>
            </View>
        );
    }
    return (
        <View style={s.wrap}>
            {imageComponent}
            <View style={s.buttonBox}>
                <View style={s.buttonWrap}>
                    <TaxiButton
                        title={btnPhotoText}
                        elColor="#E2E2EC"
                        titleColor="#282C38"
                        height={36}
                        handler={() => openGallery()}
                        isFilled={false}
                    />
                </View>
            </View>
        </View>
    );
};

UploadPhoto.propTypes = {
    setImg: PropTypes.func,
};

export default UploadPhoto;
