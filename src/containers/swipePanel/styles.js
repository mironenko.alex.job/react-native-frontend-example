import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    swipePanel: {
        flex: 1,
        backgroundColor: 'papayawhip',
        alignItems: 'center',
        justifyContent: 'center',
    },
    btnPlace: {
        width: '100%',
        display: 'flex',
        justifyContent: 'center',
        position: 'absolute',
        bottom: 25,
        zIndex: 99999,
        elevation: 99999,
        paddingRight: 30,
        paddingLeft: 30,
    },

    btnPlaceClose: {
        width: '100%',
        display: 'flex',
        justifyContent: 'center',
        position: 'absolute',
        bottom: 25,
        zIndex: 0,
        elevation: 0,
        paddingRight: 30,
        paddingLeft: 30,
    },
});

export default styles;
