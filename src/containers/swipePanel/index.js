import React, { useState } from 'react';
import PropTypes from 'prop-types';

import {
    Text, View
} from 'react-native';
import Animated from 'react-native-reanimated';
import BottomSheet from 'reanimated-bottom-sheet';
import TaxiButton from 'components/TaxiButton';

import { setDriver } from 'src/screens/DriverRegistration/DriverInfo/actions';
import s from './styles';

const SwipePanel = ({ children }) => {
    const [open, setOpen] = useState(false);

    const renderContent = () => (
        <View
            style={{
                backgroundColor: 'white',
                padding: 16,
                height: 450,
            }}
        >
            <Text>Swipe down to close</Text>
        </View>
    );

    const sheetRef = React.useRef(null);

    const slideUp = () => {
        sheetRef.current.snapTo(0);
        setOpen(false);
    };

    return (
        <>
            {children}
            <View style={open ? s.btnPlace : s.btnPlaceClose}>
                <TaxiButton
                    title="Заказать авто"
                    elColor="#FECD0E"
                    handler={() => slideUp()}
                />
            </View>
            <BottomSheet
                ref={sheetRef}
                snapPoints={[450, 300, 0]}
                borderRadius={10}
                renderContent={renderContent}
                onCloseEnd={() => setOpen(true)}
            />
        </>
    );
};

SwipePanel.propTypes = {
    children: PropTypes.element,
};

export default SwipePanel;
