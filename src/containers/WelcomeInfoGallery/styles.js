import { StyleSheet } from 'react-native';
import sGlobal from 'global/styles';

const styles = StyleSheet.create({
    arrowBtn: {
        color: sGlobal.taxiColor.color,
        fontSize: 50,
    },
    title: {
        marginTop: 40,
        fontFamily: 'SegoeuiBold',
        fontSize: 18,
        color: '#242A38',
        textAlign: 'center',
    },
    text: {
        marginTop: 10,
        fontFamily: 'Segoeui',
        fontSize: 14,
        color: '#282C38',
        textAlign: 'center',
        lineHeight: 19,
    },
    textBox: {
        paddingLeft: 30,
        paddingRight: 30,
    },
    buttonWrap: {
        marginLeft: 30,
        marginRight: 30,
    },
});
export default styles;
