import React from 'react';
import PropTypes from 'prop-types';
import {
    Image, View, Dimensions, Text,
} from 'react-native';
import Swiper from 'react-native-swiper';

import Page from 'components/Page';
import sGlobal from 'global/styles';
import TaxiButton from 'components/TaxiButton';
import { navigate } from 'utils/navigationHelper';
import s from './styles';

const { width: screenWidth } = Dimensions.get('window');

const RenderComponent = (properties, i) => (
    <View
        key={`welcome_img_${i}`}
        style={{
            backgroundColor: 'transparent',
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
        }}
    >
        <Image source={properties.url} />
        <View style={s.textBox}>
            <Text style={s.title}>{properties.title}</Text>
            <Text style={s.text}>{properties.text}</Text>
        </View>
    </View>
);

const WelcomeInfoGallery = (props) => {
    const { images } = props;
    let content = null;
    if (images && images.length > 0) {
        const btnMargin = screenWidth / 2 - 80;
        const imagesContent = images.map((image, i) => RenderComponent(image, i));
        content = (
            <Swiper
                activeDotColor={sGlobal.taxiColor.color}
                loop={false}
                showsButtons
                containerStyle={{ height: '80%', flex: 0 }}
                buttonWrapperStyle={{
                    top: 300,
                }}
                paginationStyle={{
                    position: 'absolute',
                    top: 630,
                }}
                nextButton={<Text style={[s.arrowBtn, { marginRight: btnMargin }]}>›</Text>}
                prevButton={<Text style={[s.arrowBtn, { marginLeft: btnMargin }]}>‹</Text>}
            >
                {imagesContent}
            </Swiper>
        );
    }
    return (
        <Page>
            {content}
            <View style={[s.buttonWrap]}>
                <TaxiButton
                    title="НАЧАТЬ"
                    elColor="#FECD0E"
                    handler={() => navigate('region')}
                />
            </View>
        </Page>
    );
};

WelcomeInfoGallery.propTypes = {
    images: PropTypes.array,
};

export default WelcomeInfoGallery;
