import { StyleSheet } from 'react-native';
import gStyles from 'global/styles';

const styles = StyleSheet.create({
    header: {
        backgroundColor: gStyles.taxiColor.color,
        // zIndex: 999,
        // paddingTop: 20,
        height: 135,
        // width: '100%',
        // display: 'flex',
        // flexDirection: 'row',
        // alignItems: 'center',
    },
    btnIcon: {
        width: 26,
        marginLeft: 10,
        // backgroundColor: 'red',
    },
    menuItem: {

    },
    menuItemText: {
        fontFamily: 'Segoeui',
        fontWeight: '600',
        fontSize: 16,
        color: '#282C38',
        textAlign: 'left',
    },
});
export default styles;
