import React from 'react';
import { View, Text, Image } from 'react-native';
import {
    DrawerContentScrollView,
    DrawerItem,
} from '@react-navigation/drawer';

import Info from 'img/drawer/info.png';
import OnlineReg from 'img/drawer/onlineReg.png';
import Safety from 'img/drawer/safety.png';
import Settings from 'img/drawer/settings.png';
import Support from 'img/drawer/support.png';
// import Logout from 'containers/Logout';
import styles from './styles';

function SideBar(props) {
    return (
        <View style={{ width: '100%', height: '100%', position: 'relative' }}>
            <View style={styles.header} />
            <DrawerContentScrollView {...props}>
                <DrawerItem
                    label="Безопасность"
                    icon={() => <Image source={Safety} style={styles.btnIcon} />}
                    onPress={() => props.navigation.navigate('dashboard')}
                    style={styles.menuItem}
                    labelStyle={styles.menuItemText}
                />
                <DrawerItem
                    label="Настройки"
                    icon={() => <Image source={Settings} style={styles.btnIcon} />}
                    onPress={() => props.navigation.navigate('dashboard')}
                    style={styles.menuItem}
                    labelStyle={styles.menuItemText}
                />
                <DrawerItem
                    label="Помощь"
                    icon={() => <Image source={Info} style={styles.btnIcon} />}
                    onPress={() => props.navigation.navigate('dashboard')}
                    style={styles.menuItem}
                    labelStyle={styles.menuItemText}
                />
                <DrawerItem
                    label="Служба поддержки"
                    icon={() => <Image source={Support} style={styles.btnIcon} />}
                    onPress={() => props.navigation.navigate('dashboard')}
                    style={styles.menuItem}
                    labelStyle={styles.menuItemText}
                />
                <DrawerItem
                    label="Онлайн-регистрация"
                    icon={() => <Image source={OnlineReg} style={styles.btnIcon} />}
                    onPress={() => props.navigation.navigate('registration-rules')}
                    style={styles.menuItem}
                    labelStyle={styles.menuItemText}
                />
            </DrawerContentScrollView>
            {/* <View style={styles.footer}> */}
            {/*    <Image */}
            {/*        style={{ width: '100%' }} */}
            {/*        resizeMode="center" */}
            {/*        source={logo} */}
            {/*    /> */}
            {/* </View> */}
        </View>
    );
}

export default SideBar;
