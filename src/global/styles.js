import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    safeArea: {
        flex: 1,
        justifyContent: 'center',
    },
    flexCenterElements: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    taxiColor: {
        color: '#FECD0E',
    },
    bold: {
        fontWeight: 'bold',
    },
});
export default styles;
