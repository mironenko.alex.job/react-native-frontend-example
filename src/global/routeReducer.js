import { SET_ROUTE } from './constants';
// Initial State
export const initialState = {
    data: null,
};

const routeReducer = (state = initialState, action) => {
    switch (action.type) {
    case SET_ROUTE: {
        return {
        // State
            ...state,
            // Redux Store
            data: action.route,
        };
    }
    // Default
    default: {
        return state;
    }
    }
};

export default routeReducer;
