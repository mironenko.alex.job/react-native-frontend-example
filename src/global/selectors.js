/**
 * App selectors
 */

import { createSelector } from 'reselect';

import { initialState as routeInitialState } from './routeReducer';
import { initialState as secureInitialState } from './userReducer';

const selectRoute = (state) => state.route || routeInitialState;
const selectSecure = (state) => state.secure || secureInitialState;

const selectTargetRoute = () => createSelector(selectRoute, (routeState) => {
    const { data } = routeState;
    return data && data.routeNames ? data.routeNames[data.index] : 'time-line';
});

const selectAuthToken = () => createSelector(selectSecure, (secureState) => secureState.authToken);

export {
    selectTargetRoute,
    selectAuthToken,
};
