// eslint-disable-next-line import/prefer-default-export
export const LOGOUT = 'app/LOGOUT';
export const SET_AUTH_TOKEN = 'app/SET_AUTH_TOKEN';
export const SET_ROUTE = 'app/SET_ROUTE';
