/*
 * Application Actions
 *
 * Actions change things in your application
 * Since this application uses a uni-directional data flow, specifically redux,
 * we have these actions which are the only way your application interacts with
 * your application state. This guarantees that your state is up to date and nobody
 * messes it up weirdly somewhere.
 *
 * To add a new Action:
 * 1) Import your constant
 * 2) Add a function like this:
 *    export function yourAction(var) {
 *        return { type: YOUR_ACTION_CONSTANT, var: var }
 *    }
 */

import {
    SET_AUTH_TOKEN,
    SET_ROUTE,
    LOGOUT,
} from './constants';

/**
 * Set authToken if user authenticated successfully
 *
 * @param token string
 */

export function setAuthToken(token) {
    return {
        type: SET_AUTH_TOKEN,
        token,
    };
}

/**
 * Set route object
 *
 * @param route {object} route data *
 */

export function setRoute(route) {
    return {
        type: SET_ROUTE,
        route,
    };
}

export function logOut() {
    return {
        type: LOGOUT,
    };
}
