import React from 'react';
import PropTypes from 'prop-types';
import { View, Text } from 'react-native';
import { createDrawerNavigator } from '@react-navigation/drawer';

import Dashboard from 'screens/Dashboard';
import DriverRegistration from 'screens/DriverRegistration';
import WelcomeInfo from 'screens/WelcomeInfo';

import Region from 'screens/Registration/Region';
import PhoneNumber from 'screens/Registration/PhoneNumber';

import RegistrationRules from 'screens/DriverRegistration/RegistrationRules/index';
import PhotoRules from 'screens/DriverRegistration/PhotoRules/index';
import DriverInfo from 'screens/DriverRegistration/DriverInfo/index';
import CarModel from 'screens/DriverRegistration/CarModel/index';
import CarColor from 'screens/DriverRegistration/CarColor/index';
import CarLicense from 'screens/DriverRegistration/CarLicense';
import CarLicenseAdditional from 'screens/DriverRegistration/CarLicenseAdditional';

import DrawerComponent from 'containers/DrawerComponent';
import gStyles from 'global/styles';

const Drawer = createDrawerNavigator();

function Routes(props) {
    const { authToken } = props;
    let screens = <></>;
    if (!authToken) {
        screens = (
            <>
                <Drawer.Screen
                    name="welcome-info"
                    component={WelcomeInfo}
                />
                <Drawer.Screen
                    name="region"
                    component={Region}
                    options={
                        { title: 'Выберите страну или регион' }
                    }
                />
                <Drawer.Screen
                    name="set-phone"
                    component={PhoneNumber}
                    options={
                        { title: 'Введите ваш номер телефона для входа' }
                    }
                />
                <Drawer.Screen
                    name="dashboard"
                    component={Dashboard}
                />
                <Drawer.Screen
                    name="registration-rules"
                    component={RegistrationRules}
                />
                <Drawer.Screen
                    name="photo-rules"
                    component={PhotoRules}
                />
                <Drawer.Screen
                    name="driver-info"
                    component={DriverInfo}
                />
                <Drawer.Screen
                    name="car-model"
                    component={CarModel}
                />
                <Drawer.Screen
                    name="car-color"
                    component={CarColor}
                />
                <Drawer.Screen
                    name="car-license"
                    component={CarLicense}
                />
                <Drawer.Screen
                    name="car-license-additional"
                    component={CarLicenseAdditional}
                />
            </>
        );

        return (
            <Drawer.Navigator
                drawerContent={(dprops) => DrawerComponent(dprops)}
                initialRouteName="dashboard"
            >
                {screens}
            </Drawer.Navigator>
        );
    }
}

Routes.propTypes = {
    authToken: PropTypes.string,
};
export default Routes;
