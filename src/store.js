import thunk from 'redux-thunk';
import createSecureStore from 'redux-persist-expo-securestore';
import {
    createStore, applyMiddleware, compose, combineReducers,
} from 'redux';
import createSagaMiddleware from 'redux-saga';
// import { MODE } from 'react-native-dotenv';

// eslint-disable-next-line import/no-extraneous-dependencies
// import { createLogger } from 'redux-logger';
import { LOGOUT } from 'global/constants';
import { persistReducer, persistStore } from 'redux-persist';

import routeReducer from 'global/routeReducer';
import regionReducer from 'screens/Registration/Region/reducer';
import phoneReducer from 'screens/Registration/PhoneNumber/reducer';
import driverReducer from 'screens/DriverRegistration/DriverInfo/reducer';
import carModelReducer from 'screens/DriverRegistration/CarModel/reducer';
import carColorReducer from 'screens/DriverRegistration/CarColor/reducer';
import carLicenseReducer from 'screens/DriverRegistration/CarLicense/reducer';
import carPhotosReducer from 'screens/DriverRegistration/CarLicenseAdditional/reducer';

import secureReducer from 'global/userReducer';

const secureStore = createSecureStore();

const sagaMiddleware = createSagaMiddleware();
const middleware = [thunk, sagaMiddleware];
const composeEnhancers = compose;
// const composeEnhancers = MODE === 'dev'
//   ? composeWithDevTools({
//     realtime: true,
//     host: 'localhost',
//     port: 8000,
//   })
//   : compose;
const enhancers = [applyMiddleware(...middleware)];

const secureConfig = {
    key: 'secure',
    version: 1,
    storage: secureStore,
};

const appReducer = combineReducers({
    route: routeReducer,
    user: combineReducers({
        region: regionReducer,
        phone: phoneReducer,
        driver: driverReducer,
        model: carModelReducer,
        color: carColorReducer,
        license: carLicenseReducer,
        photos: carPhotosReducer,
    }),
    secure: persistReducer(secureConfig, secureReducer),
});

const rootReducer = (state, action) => {
    if (action.type === LOGOUT) {
        secureStore.removeItem('persist:secure');
        state = undefined;
    }

    return appReducer(state, action);
};

const store = createStore(rootReducer, composeEnhancers(...enhancers));
// @ts-ignore
store.runSaga = sagaMiddleware.run;
// @ts-ignore
store.injectedReducers = {}; // Reducer registry
// @ts-ignore
store.injectedSagas = {}; // Saga registry

// store.runSaga(loadingKidsSaga);
// store.runSaga(rootSaga);

const persistor = persistStore(store);

export { persistor, store };
