import React from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';

import s from './styles';

const Page = (props) => {
    const { bg, children } = props;
    const wrapStyles = {};
    if (bg) {
        wrapStyles.backgroundColor = bg;
    }
    return (<View style={[s.container, wrapStyles]}>{children}</View>);
}

Page.propTypes = {
    bg: PropTypes.string,
    children: PropTypes.any,
}
export default Page;
