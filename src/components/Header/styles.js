import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    header: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        backgroundColor: '#fff',
        padding: 15,
        paddingTop: 67,
        borderBottomColor: '#E0E0E7',
        borderBottomWidth: 1,
    },
});

export default styles;
