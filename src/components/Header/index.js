import React from 'react';
import {Text, View} from 'react-native';

import s from './styles';

const Header = (props) => {
    const {
        left,
        center,
        right,
    } = props;
    return (
        <View style={s.header}>
            {left}
            {center}
            {right}
        </View>
    );
}
export default Header;
