import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    textWrap: {
        padding: 10,
        paddingTop: 70,
        paddingRight: 40,
        marginLeft: 10,
        backgroundColor: 'transparent',
    },
});
export default styles;
