import React from 'react';
import { Text, View } from 'react-native';
import { AntDesign } from '@expo/vector-icons';

import { getColor } from '../handlers';
import s from '../styles';

function getName(type) {
    let name = 'questioncircleo';
    switch (type) {
    case 'success':
        name = 'checkcircleo';
        break;
    case 'danger':
        name = 'closecircleo';
        break;
    case 'warning':
        name = 'exclamationcircleo';
        break;
    default:
        break;
    }
    return name;
}

function FlashIcon(props) {
    const { type } = props;
    let icon = null;
    if (type) {
        icon = (
            <AntDesign
                name={getName(type)}
                size={24}
                color="#fff"
                style={{ marginRight: 10 }}
            />
        );
    }
    return (icon);
}
export default FlashIcon;
