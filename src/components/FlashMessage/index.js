import React from 'react';
import { Text, View } from 'react-native';
import FlashMessage from 'react-native-flash-message';

import s from './styles';

import FlashIcon from './FlashIcon';
import { getColor } from './handlers';

function FlashMessageContent(props) {
    const { message, textStyle = {}, titleStyle = {} } = props;
    let flash = null;
    if (message) {
        const { description, type, message: text } = message;
        flash = (
            <View style={{ backgroundColor: getColor(type) }}>
                <View style={s.textWrap}>
                    <View style={{ flex: 1, flexDirection: 'row' }}>
                        <FlashIcon type={type} />
                        <Text style={[titleStyle]}>{text}</Text>
                    </View>

                    <Text style={[textStyle, { paddingLeft: 34 }]}>{description}</Text>
                </View>
            </View>
        );
    }
    return (flash);
}

function FlashMessageElement() {
    return (
        <View style={{ flex: 1, zIndex: 50 }}>
            <FlashMessage
                position="top"
                duration={2000}
                MessageComponent={(mProps) => <FlashMessageContent {...mProps} />}
            />
        </View>
    );
}
export default FlashMessageElement;
