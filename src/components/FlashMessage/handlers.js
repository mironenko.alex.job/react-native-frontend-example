import { showMessage } from 'react-native-flash-message';

export function CustomTransition(animValue) {
    const opacity = animValue.interpolate({
        inputRange: [0, 1],
        outputRange: [0, 1],
    });

    const translateX = animValue.interpolate({
        inputRange: [0, 1],
        outputRange: [360, 0],
    });

    return {
        transform: [{ translateX }],
        opacity,
    };
}

const defaultMessageOptions = {
    message: '',
    description: '',
    type: 'default',
    // transitionConfig: CustomTransition,
    position: { top: 0, left: 0, right: 0 },
    titleStyle: { color: '#fff', fontSize: 14, paddingTop: 5 },
    textStyle: { color: '#fff', fontSize: 14 },
};

export function onOpenFlash(props = {}) {
    const opts = Object.assign(defaultMessageOptions, props);
    showMessage(opts);
}

export function getColor(type) {
    let color = '#fff';
    switch (type) {
    case 'success':
        color = '#47C65E';
        break;
    case 'danger':
        color = '#FD5F5F';
        break;
    case 'warning':
        color = '#FFD800';
        break;
    default:
        break;
    }
    return color;
}
