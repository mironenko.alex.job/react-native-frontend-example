import React from 'react';
import PropTypes from 'prop-types';
import { Text, TouchableOpacity, View } from 'react-native';
import { openInDevelopmentAlert } from 'utils/alertHelper';
import s from './styles';

const TaxiButton = (props) => {
    const {
        elColor,
        title,
        titleColor,
        isFilled = true,
        handler,
        height,
    } = props;
    const sButton = (elColor) ? { backgroundColor: elColor } : {};
    const sTitle = (titleColor) ? { color: titleColor, textTransform: 'uppercase', fontWeight: 'bold' } : { textTransform: 'uppercase', fontWeight: 'bold' };
    const sSize = (height) ? { height } : { height: 60 };
    if (!isFilled) {
        sTitle.textTransform = 'capitalize';
        sTitle.fontWeight = 'normal';
        sButton.backgroundColor = 'transparent';
        sButton.borderWidth = 1;
        sButton.borderColor = elColor;
    }
    return (
        <View style={s.buttonWrap}>
            <TouchableOpacity
                style={[s.button, sButton, sSize]}
                onPress={() => {
                    if (handler) {
                        handler();
                    } else {
                        openInDevelopmentAlert();
                    }
                }}
            >
                <Text style={[s.title, sTitle]}>{title}</Text>
            </TouchableOpacity>
        </View>
    );
};

TaxiButton.propTypes = {
    handler: PropTypes.func,
    isFilled: PropTypes.bool,
    elColor: PropTypes.string,
    title: PropTypes.string,
    titleColor: PropTypes.string,
    height: PropTypes.number,
};

export default TaxiButton;
