import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    buttonWrap: {
        width: '100%',
    },
    button: {
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#FECD0E',
        borderRadius: 50,
    },
    title: {
        fontFamily: 'Segoeui',
        textAlign: 'center',
        fontStyle: 'normal',
        fontSize: 14,
        color: '#fff',
    },
});
export default styles;
