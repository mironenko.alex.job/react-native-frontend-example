import {
    SET_PHONE,
} from './constants';

/**
 * Set ohone
 *
 * @param phone string
 */

export function setPhone(phone) {
    return {
        type: SET_PHONE,
        phone,
    };
}