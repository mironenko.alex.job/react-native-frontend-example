import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Image, Text, View } from 'react-native';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import { TextInputMask } from 'react-native-masked-text';
import DropDownPicker from 'react-native-dropdown-picker';

import TaxiButton from 'components/TaxiButton';
import { navigate } from 'utils/navigationHelper';

import { setPhone } from './actions';
import { selectPhone } from './selectors';

import s from './styles';

function getMasK(code) {
    const masks = {
        '+7': '(999) 999-99-99',
        '+375': '(99) 999-99-99',
    };
    return masks[code];
}

const countries = [
    {
        label: '',
        value: '+7',
        icon: () => <Image source={require('img/flags/rus.png')} />,
    },
    {
        label: '',
        value: '+375',
        icon: () => <Image source={require('img/flags/brus.png')} />,
    }];

const PhoneNumber = (props) => {
    const [countryCode, setCountryCode] = useState('+7');
    const { dispatch, phone } = props;
    // const [pNum, setPNum] = useState('');

    const onSetPhone = (val) => {
        dispatch(setPhone(val));
    };

    return (
        <View style={{ flex: 1, backgroundColor: '#fff' }}>
            <View style={s.header}>
                <Text style={s.headerText}>Введите ваш номер телефона для входа</Text>
            </View>
            <View style={s.inputMaskWrapper}>
                <DropDownPicker
                    items={countries}
                    defaultValue={countryCode}
                    containerStyle={s.dropDownContainerStyle}
                    style={s.dropDownEl}
                    itemStyle={s.dropDownItemStyle}
                    dropDownStyle={s.dropDown}
                    onChangeItem={(item) => setCountryCode(item.value)}
                />
                <View style={s.codeWrap}>
                    <Text style={s.code}>{countryCode}</Text>
                </View>
                <TextInputMask
                    style={s.inputMask}
                    type="custom"
                    placeholder="Ваш номер телефона"
                    placeholderTextColor="#A1A1AF"
                    options={{ mask: getMasK(countryCode) }}
                    value={phone}
                    onChangeText={(text) => onSetPhone(text)}
                />
            </View>
            <View style={s.buttonWrap}>
                <TaxiButton
                    title="ДАЛЕЕ"
                    elColor="#FECD0E"
                    handler={() => navigate('dashboard')}
                />
            </View>
        </View>
    );
};

PhoneNumber.propTypes = {
    dispatch: PropTypes.func,
    phone: PropTypes.string,
};

const mapStateToProps = createStructuredSelector({
    phone: selectPhone(),
});

export default connect(mapStateToProps, null)(PhoneNumber);
