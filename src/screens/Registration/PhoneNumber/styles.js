import { StyleSheet } from 'react-native';
import sStyles from "global/styles";

const styles = StyleSheet.create({
    inputMaskWrapper: {
        paddingTop: 54,
        flexDirection: 'row',
        zIndex: 2,
        paddingLeft: 30,
        paddingRight: 30,
    },
    inputMask: {
        color: '#282C38',
        fontFamily: 'Segoeui',
        fontSize: 18,
        paddingTop: 5,
        width: 180,
        marginLeft: 5,
        borderBottomWidth: 1,
        borderBottomColor: '#A1A1AF',
    },
    codeWrap: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    code: {
        color: '#282C38',
        paddingTop: 5,
        fontSize: 18,
    },
    dropDownEl: {
        backgroundColor: '#fff',
        borderWidth: 0,
    },
    dropDownContainerStyle: {
        height: 40,
        width: 100,
    },
    dropDownItemStyle: {
        justifyContent: 'flex-start',
    },
    dropDown: {
        backgroundColor: '#fafafa',
    },
    buttonWrap: {
        marginTop: 48,
        paddingRight: 25,
        paddingLeft: 25,
    },
    header: {
        backgroundColor: sStyles.taxiColor.color,
        paddingTop: 67,
        paddingBottom: 22,
    },
    headerText: {
        fontFamily: 'Helvetica',
        fontWeight: 'bold',
        color: '#fff',
        textAlign: 'center',
        fontSize: 18,
    },
});
export default styles;
