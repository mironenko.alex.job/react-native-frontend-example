import produce from 'immer';
import { SET_PHONE } from './constants';
// Initial State
export const initialState = {
    val: null,
};

const phoneReducer = (state = initialState, action) => produce(state, (draft) => {
    switch (action.type) {
    case SET_PHONE:
        draft.val = action.phone;
        break;
    default: {
        return state;
    }
    }
});

export default phoneReducer;
