/**
 * Phone selectors
 */

import { createSelector } from 'reselect';
import { initialState } from './reducer';

const phoneState = (state) => state.user.phone || initialState;

const selectPhone = () => createSelector(phoneState, (pState) => pState.val);

// eslint-disable-next-line import/prefer-default-export
export { selectPhone };
