import {
    SET_REGION,
} from './constants';

/**
 * Set region
 *
 * @param region string
 */

export function setRegion(region) {
    return {
        type: SET_REGION,
        region,
    };
}