/**
 * Region selectors
 */

import { createSelector } from 'reselect';
import { initialState } from './reducer';

const regionState = (state) => state.user.region || initialState;

const selectRegion = () => createSelector(regionState, (rState) => rState.val);

// eslint-disable-next-line import/prefer-default-export
export { selectRegion };
