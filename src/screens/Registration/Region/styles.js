import { StyleSheet } from 'react-native';

import sStyles from 'global/styles';

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'transparent',
        flex: 1,
    },
    autocompleteContainer: {
        marginLeft: 0,
        marginRight: 0,
        marginBottom: 30,
        marginTop: 25,
        backgroundColor: 'transparent',
    },
    searchInput: {
        borderRadius: 50,
        backgroundColor: '#fff',
        height: 45,
        paddingLeft: 20,
        paddingRight: 20,
        marginLeft: 22,
        marginRight: 22,
    },
    listText: {
        fontWeight: 'normal',
        fontFamily: 'Segoeui',
        fontSize: 14,
    },
    header: {
        backgroundColor: sStyles.taxiColor.color,
        paddingTop: 67,
        paddingBottom: 22,
    },
    headerText: {
        fontFamily: 'Helvetica',
        fontWeight: 'bold',
        color: '#fff',
        textAlign: 'center',
        fontSize: 18,
    },
});

export default styles;
