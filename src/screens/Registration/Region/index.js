import React from 'react';
import PropTypes from 'prop-types';
import {
    Text, View, TouchableOpacity, TextInput,
} from 'react-native';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import Autocomplete from 'react-native-autocomplete-input';
import { ListItem } from 'react-native-elements';
import { navigate } from 'utils/navigationHelper';
import { setRegion } from './actions';
import { selectRegion } from './selectors';

import s from './styles';

const placesList = [{ id: 1, title: 'Белгородская область' }, { id: 2, title: 'Брянская область' }];
const comp = (a, b) => a.toLowerCase().trim() === b.toLowerCase().trim();

const Region = (props) => {
    const { dispatch, region } = props;

    const onQueryPlaces = () => {
        if (region === null || region === '') {
            return placesList;
        }

        const regex = new RegExp(`${region.trim()}`, 'i');
        return placesList.filter((place) => place.title.search(regex) >= 0);
    };
    const targetPlaces = onQueryPlaces();

    const onSetRegion = (val) => {
        dispatch(setRegion(val));
    };
    return (
        <View style={s.container}>
            <View style={s.header}>
                <Text style={s.headerText}>Выберите страну или регион</Text>
            </View>
            <Autocomplete
                inputContainerStyle={{ borderWidth: 0, borderRadius: 30, backgroundColor: 'transparent' }}
                listContainerStyle={{ marginTop: 30, paddingLeft: 0 }}
                listStyle={{
                    width: '100%', paddingLeft: 0, paddingRight: 0, marginLeft: 0, marginRight: 0,
                }}
                autoCapitalize="none"
                autoCorrect={false}
                containerStyle={s.autocompleteContainer}
                data={targetPlaces.length === 1 && comp(region, targetPlaces[0].title)
                    ? []
                    : targetPlaces}
                defaultValue={region}
                onChangeText={(text) => onSetRegion(text)}
                placeholder="Название страны или региона"
                renderItem={(place) => (
                    <TouchableOpacity onPress={() => {
                        const regionVal = place.item.title;
                        onSetRegion(regionVal);
                        setTimeout(() => navigate('set-phone'), 1000);
                    }}
                    >
                        <ListItem
                            title={(
                                <Text>
                                    <Text style={s.listText}>{place.item.title}</Text>
                                </Text>
                            )}
                            bottomDivider
                        />
                    </TouchableOpacity>
                )}
                renderTextInput={(inputProps) => (
                    <TextInput
                        {...inputProps}
                        style={s.searchInput}
                    />
                )}
            />
        </View>
    );
};

Region.propTypes = {
    dispatch: PropTypes.func,
    region: PropTypes.string,
};

const mapStateToProps = createStructuredSelector({
    region: selectRegion(),
});

export default connect(mapStateToProps, null)(Region);
