import produce from 'immer';
import { SET_REGION } from './constants';
// Initial State
export const initialState = {
    val: null,
};

const regionReducer = (state = initialState, action) => produce(state, (draft) => {
    switch (action.type) {
    case SET_REGION:
        draft.val = action.region;
        break;
    default: {
        return state;
    }
    }
});

export default regionReducer;
