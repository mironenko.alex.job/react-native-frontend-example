import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#F1F2F6',
    },
    wrap: {
        width: '100%',
        display: 'flex',
        flexDirection: 'column',
    },
    greyWrap: {
        paddingTop: 33,
        paddingBottom: 28,
        paddingRight: 26,
        paddingLeft: 26,
        backgroundColor: '#F1F2F6',
    },
    imgBlock: {
        width: '100%',
        display: 'flex',
        alignItems: 'center',
    },
    img: {
        width: 173,
        height: 173,
        borderRadius: 173,
        backgroundColor: '#F4F3F0',
        marginBottom: 28,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
    },
    list: {
        display: 'flex',
        alignItems: 'center',
        flexDirection: 'row',
        marginBottom: 15,
        paddingLeft: 30,
        paddingRight: 3,
    },
    listText: {
        fontFamily: 'Segoeui',
        fontSize: 15,
        lineHeight: 20,
        color: '#242A38',
        marginLeft: 10,
    },
    buttonWrap: {
        marginTop: 28,
        paddingRight: 20,
        paddingLeft: 20,
        marginBottom: 40,
    },
});

export default styles;
