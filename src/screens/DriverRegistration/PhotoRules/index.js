import React from 'react';
import { Text, View, Image } from 'react-native';

import Page from 'components/Page';
import Header from 'components/Header';
import { buttonBuild } from 'utils/buttonsHelper';
import TaxiButton from 'components/TaxiButton';
import { navigate } from 'utils/navigationHelper';

import List from 'img/list.png';
import Photo from 'img/photoExample.png';

import s from './styles';

const PhotoRules = (props) => {
    // const { } = props;
    const left = buttonBuild(
        () => navigate('registration-rules'),
        'Назад',
        { color: '#FF8901' },
    );
    const center = <Text>Требования к фото</Text>;
    const right = buttonBuild(
        () => navigate('dashboard'),
        'Закрыть',
        { color: '#A1A1AF' },
    );
    const data = [
        { key: 'Отчетливо видно лицо' },
        { key: 'Без солнцезащитных очков' },
        { key: 'Хорошее освещение и без фильтров' },
    ];
    const list = data.map((item, i) => (
        <View key={`list${i}`} style={[s.list]}>
            <Image source={List} />
            <Text style={[s.listText]}>{item.key}</Text>
        </View>
    ));
    return (
        <Page bg="transparent">
            <Header left={left} center={center} right={right} />
            <View style={[s.wrap, s.greyWrap]}>
                <View style={[s.imgBlock]}>
                    <Image style={[s.img]} source={Photo} />
                </View>
                {list}
                <View style={[s.buttonWrap]}>
                    <TaxiButton
                        title="далее"
                        elColor="#FECD0E"
                        handler={() => navigate('driver-info')}
                    />
                </View>
            </View>
        </Page>
    );
};
export default PhotoRules;
