import { StyleSheet, Dimensions } from 'react-native';

const { width } = Dimensions.get('window');

const styles = StyleSheet.create({
    carModelWrap: {
        borderBottomColor: '#E0E0E7',
        borderBottomWidth: 1,
        backgroundColor: '#fff',
        paddingTop: 30,
        paddingBottom: 30,
    },
    carModel: {
        fontFamily: 'SegoeuiBold',
        fontSize: 20,
        lineHeight: 27,
        textAlign: 'center',
        paddingBottom: 10,
    },
    carColor: {
        color: '#FECD0E',
        textAlign: 'center',
    },
    licenseNum: {
        fontFamily: 'Segoeui',
        fontSize: 16,
        lineHeight: 21,
        color: '#282C38',
        textAlign: 'center',
        marginTop: 15,
    },
    buttonWrap: {
        marginLeft: width / 2 - 90,
        width: 180,
        marginTop: 25,
    },
    saveButtonWrap: {
        padding: 25,
        paddingBottom: 40,
        backgroundColor: '#fff',
    },
});

export default styles;
