import produce from 'immer';
import { SET_CAR_PHOTOS } from './constants';
// Initial State
export const initialState = {
    photos: {
        car: null,
        carLicense: null,
        carLicenseBack: null,
    },
};

const carPhotosReducer = (state = initialState, action) => produce(state, (draft) => {
    switch (action.type) {
    case SET_CAR_PHOTOS:
        draft.val = action.photos;
        break;
    default: {
        return state;
    }
    }
});

export default carPhotosReducer;
