import React, { useState } from 'react';
import { ScrollView, Text, View } from 'react-native';

import { buttonBuild } from 'utils/buttonsHelper';
import FlashMessageElement from 'components/FlashMessage';
import { onOpenFlash } from 'components/FlashMessage/handlers';
import Header from 'components/Header';
import Page from 'components/Page';
import TaxiButton from 'components/TaxiButton';
import CarLicensePhoto from 'containers/CarLicensePhoto';
import { navigate } from 'utils/navigationHelper';
import s from 'src/screens/DriverRegistration/CarLicenseAdditional/styles';
import PropTypes from 'prop-types';
import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';
import { selectCarColor } from '../CarColor/selectors';
import { selectCarModel } from '../CarModel/selectors';
import { selectCarLicense } from '../CarLicense/selectors';
import { selectCarPhotos } from './selectors';
import {setPhotos} from "./actions";

const CarLicenseAdditional = (props) => {
    const {
        carColor, carModel, carLicense, carPhotos, dispatch,
    } = props;
    const [uriCar, setUriCar] = useState(carPhotos.car);
    const [uriCarLic, setUriCarLic] = useState(carPhotos.carLicense);
    const [uriCarLicBack, setUriCarLicBack] = useState(carPhotos.carLicenseBack);

    const left = buttonBuild(
        () => navigate('car-license'),
        'Назад',
        { color: '#FF8901' },
    );
    const center = <Text>Госномер</Text>;
    const right = buttonBuild(
        () => navigate('dashboard'),
        'Закрыть',
        { color: '#A1A1AF' },
    );

    const onSubmit = () => {
        const data = {
            car: uriCar,
            carLicense: uriCarLic,
            carLicenseBack: uriCarLicBack,
        };
        console.log('Submit', data);
        dispatch(setPhotos(data));
        navigate('dashboard');
    };
    // console.log('Props', props);
    return (
        <Page bg="transparent">
            <FlashMessageElement />
            <Header left={left} center={center} right={right} />
            <ScrollView>
                <View style={s.carModelWrap}>
                    <Text style={s.carModel}>{carModel}</Text>
                    <Text style={[s.carColor, { color: carColor.hex }]}>{carColor.name}</Text>
                    <Text style={s.licenseNum}>{carLicense}</Text>
                    <View style={[s.buttonWrap]}>
                        <TaxiButton
                            title="Редактировать"
                            elColor="#FECD0E"
                            titleColor="#FECD0E"
                            handler={() => navigate('dashboard')}
                            isFilled={false}
                        />
                    </View>
                </View>
                <CarLicensePhoto
                    showSplash
                    title="Сфотографируйте машину как на примере, чтобы был виден госномер"
                    setImg={(uri) => setUriCar(uri)}
                    uri={uriCar}
                />
                <CarLicensePhoto
                    title="Свидетельство о регистрации ТС. Сторона, где указан госномер"
                    setImg={(uri) => setUriCarLic(uri)}
                    uri={uriCarLic}
                />
                <CarLicensePhoto
                    title="Свидетельство о регистрации ТС. (обратная сторона)"
                    setImg={(uri) => setUriCarLicBack(uri)}
                    uri={uriCarLicBack}
                />
                <View style={[s.saveButtonWrap]}>
                    <TaxiButton
                        title="Сохранить"
                        elColor="#FECD0E"
                        handler={() => {
                            const opts = {
                                type: 'success',
                                message: 'Данные успешно сохранены!',
                            };
                            onOpenFlash(opts);
                            onSubmit();
                        }}
                    />
                </View>
            </ScrollView>
        </Page>
    );
};
CarLicenseAdditional.propTypes = {
    dispatch: PropTypes.func,
    carColor: PropTypes.object,
    carModel: PropTypes.string,
    carLicense: PropTypes.string,
    carPhotos: PropTypes.object,
};

const mapStateToProps = createStructuredSelector({
    carColor: selectCarColor(),
    carModel: selectCarModel(),
    carLicense: selectCarLicense(),
    carPhotos: selectCarPhotos(),
});

export default connect(mapStateToProps, null)(CarLicenseAdditional);
