/**
 * Car Photos selectors
 */

import { createSelector } from 'reselect';
import { initialState } from './reducer';

const carPhotosState = (state) => state.user.photos || initialState;

const selectCarPhotos = () => createSelector(carPhotosState, (pState) => pState.photos);

// eslint-disable-next-line import/prefer-default-export
export { selectCarPhotos };
