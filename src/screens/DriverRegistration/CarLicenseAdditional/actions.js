import {
    SET_CAR_PHOTOS,
} from './constants';

/**
 * Set car photos
 *
 * @param photos object
 */

export function setPhotos(photos) {
    return {
        type: SET_CAR_PHOTOS,
        photos,
    };
}