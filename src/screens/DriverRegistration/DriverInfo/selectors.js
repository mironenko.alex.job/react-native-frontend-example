/**
 * Driver info selectors
 */

import { createSelector } from 'reselect';
import { initialState } from './reducer';

const driverState = (state) => state.user.driver || initialState;

const selectDriver = () => createSelector(driverState, (dState) => dState.driver);

// eslint-disable-next-line import/prefer-default-export
export { selectDriver };
