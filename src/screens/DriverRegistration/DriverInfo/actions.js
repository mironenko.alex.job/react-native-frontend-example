import {
    SET_DRIVER,
} from './constants';

/**
 * Set driver info
 *
 * @param data object
 */

export function setDriver(data) {
    return {
        type: SET_DRIVER,
        data,
    };
}