import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#fff',
    },
    wrap: {
        width: '100%',
        display: 'flex',
        flexDirection: 'column',
    },
    whiteWrap: {
        alignItems: 'center',
        paddingTop: 32,
        paddingBottom: 10,
        backgroundColor: '#fff',
    },
    imgBlock: {
        width: 85,
        height: 85,
        borderRadius: 85,
        backgroundColor: '#F4F3F0',
        marginBottom: 32,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        borderColor: '#FFD328',
        borderWidth: 1,
    },
    inputWrap: {
        borderBottomColor: '#E0E0E7',
        borderBottomWidth: 1,
        backgroundColor: '#fff',
        paddingTop: 30,
        paddingLeft: 42,
        paddingRight: 42,
    },
    inputLabel: {
        fontFamily: 'Segoeui',
        fontSize: 14,
        color: '#282C38',
        textAlign: 'left',
    },
    input: {
        color: '#282C38',
        fontSize: 16,
    },
    radioBlocks: {
        width: '100%',
        flexDirection: 'row',
        marginTop: 20,
        marginBottom: 25,
    },
    radioBlock: {
        width: '50%',
        flexDirection: 'row',
        alignItems: 'center',
    },
    radioWrap: {
        marginRight: 10,
        borderRadius: 28,
        height: 28,
        width: 28,
        alignItems: 'center',
        justifyContent: 'center',
        borderColor: '#E0E0E7',
        borderWidth: 1,
    },
    radio: {
        borderRadius: 20,
        height: 20,
        width: 20,
        backgroundColor: '#FECD0E',
    },
    buttonWrap: {
        marginTop: 25,
        paddingRight: 25,
        paddingLeft: 25,
        marginBottom: 25,
        backgroundColor: '#fff',
    },
});

export default styles;
