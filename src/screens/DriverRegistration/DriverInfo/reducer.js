import produce from 'immer';
import { SET_DRIVER } from './constants';
// Initial State
export const initialState = {
    driver: {
        img: null,
        firstName: null,
        lsatName: null,
        female: false,
        male: true,
        birthday: null,
    },
};

const driverReducer = (state = initialState, action) => produce(state, (draft) => {
    switch (action.type) {
    case SET_DRIVER:
        draft.driver = action.driver;
        break;
    default: {
        return state;
    }
    }
});

export default driverReducer;
