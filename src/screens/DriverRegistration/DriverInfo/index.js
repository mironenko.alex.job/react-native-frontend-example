import React, { useState } from 'react';
import {
    Text, View, ScrollView, TouchableOpacity,
} from 'react-native';
import PropTypes from 'prop-types';
import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';

import { selectDriver } from 'src/screens/DriverRegistration/DriverInfo/selectors';
import { setDriver } from './actions';

import Header from 'components/Header';
import { buttonBuild } from 'utils/buttonsHelper';
import TaxiButton from 'components/TaxiButton';
import UploadPhoto from 'containers/UploadPhoto';
import { navigate } from 'utils/navigationHelper';

import { Input } from 'react-native-elements';
import s from './styles';

const DriverInfo = (props) => {
    const [female, setFemale] = useState(false);
    const [male, setMale] = useState(true);

    const [firstName, setFirstName] = useState();
    const [lastName, setLastName] = useState();

    const [birthday, setBirthday] = useState();

    const [img, setImg] = useState();

    const { dispatch, driver } = props;

    // const { } = props;
    const left = buttonBuild(
        () => navigate('photo-rules'),
        'Назад',
        { color: '#FF8901' },
    );
    const center = <Text>О себе</Text>;
    const right = buttonBuild(
        () => navigate('dashboard'),
        'Закрыть',
        { color: '#A1A1AF' },
    );

    // console.log('Props', props);

    const onSubmit = () => {
        const data = {
            img,
            birthday,
            firstName,
            lastName,
            female,
            male,
        };
        console.log('Submit', data);
        dispatch(setDriver(data));
        navigate('car-model');
    };
    // console.log('Uri', img);

    return (
        <ScrollView style={[s.container]}>
            <Header left={left} center={center} right={right} />
            <UploadPhoto setImg={(uri) => setImg(uri)} />
            <View style={s.inputWrap}>
                <Text style={s.inputLabel}>Имя</Text>
                <Input
                    inputContainerStyle={{ borderBottomColor: '#E0E0E7' }}
                    inputStyle={s.input}
                    value={firstName}
                    onChangeText={(data) => setFirstName(data)}
                    placeholder="Введите ваше имя"
                    containerStyle={{ paddingLeft: 0 }}
                />

                <Text style={s.inputLabel}>Фамилия</Text>
                <Input
                    inputContainerStyle={{ borderBottomColor: '#E0E0E7' }}
                    inputStyle={s.input}
                    value={lastName}
                    onChangeText={(data) => setLastName(data)}
                    placeholder="Введите вашу фамилию"
                    containerStyle={{ paddingLeft: 0 }}
                />

                <View style={[s.radioBlocks]}>
                    <View style={[s.radioBlock]}>
                        <TouchableOpacity
                            style={[s.radioWrap]}
                            onPress={() => { setFemale(false); setMale(true); }}
                        >
                            {male ? <View style={[s.radio]} /> : null}
                        </TouchableOpacity>
                        <Text style={s.inputLabel}>Мужской</Text>
                    </View>
                    <View style={[s.radioBlock]}>
                        <TouchableOpacity
                            style={[s.radioWrap]}
                            onPress={() => { setFemale(true); setMale(false); }}
                        >
                            {female ? <View style={[s.radio]} /> : null}
                        </TouchableOpacity>
                        <Text style={s.inputLabel}>Женский</Text>
                    </View>
                </View>

                <Text style={s.inputLabel}>День рождения </Text>
                <Input
                    inputContainerStyle={{ borderBottomColor: '#E0E0E7' }}
                    inputStyle={[s.input]}
                    placeholder="Введите дату вашего рождения"
                    value={birthday}
                    onChangeText={(data) => setBirthday(data)}
                    containerStyle={{ paddingLeft: 0 }}
                />
            </View>
            <View style={[s.buttonWrap]}>
                <TaxiButton
                    title="далее"
                    elColor="#FECD0E"
                    handler={() => onSubmit()}
                />
            </View>
        </ScrollView>
    );
};

DriverInfo.propTypes = {
    dispatch: PropTypes.func,
    driver: PropTypes.object,
};

const mapStateToProps = createStructuredSelector({
    driver: selectDriver(),
});

export default connect(mapStateToProps, null)(DriverInfo);
