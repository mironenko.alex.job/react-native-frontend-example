import produce from 'immer';
import { SET_MODEL } from './constants';
// Initial State
export const initialState = {
    val: '',
};

const carModelReducer = (state = initialState, action) => produce(state, (draft) => {
    switch (action.type) {
    case SET_MODEL:
        draft.val = action.model;
        break;
    default: {
        return state;
    }
    }
});

export default carModelReducer;
