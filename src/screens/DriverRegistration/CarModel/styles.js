import { StyleSheet } from 'react-native';

import sStyles from 'global/styles';

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#FECD0E',
        width: '100%',
        flex: 1,
    },
    autocompleteContainer: {
        width: '100%',
        marginLeft: 0,
        marginRight: 0,
        marginBottom: 30,
        marginTop: 25,
        paddingLeft: 0,
        paddingRight: 0,
    },
    searchInput: {
        borderRadius: 50,
        backgroundColor: '#fff',
        height: 45,
        paddingLeft: 20,
        paddingRight: 20,
        marginLeft: 22,
        marginRight: 22,
    },
    list: {
        width: '100%',
        marginLeft: 0,
        marginRight: 0,
        height: 61,
        alignItems: 'center',
        borderBottomWidth: 1,
        borderBottomColor: '#E2E2EC',
        flexDirection: 'row',
        paddingRight: 22,
        paddingLeft: 22,
    },
    listImg: {
        width: 30,
        height: 30,
        borderRadius: 30,
        marginRight: 10,
        marginLeft: 22,
    },
    listText: {
        fontWeight: 'normal',
        fontFamily: 'Segoeui',
        fontSize: 14,
    },
    header: {
        backgroundColor: sStyles.taxiColor.color,
        paddingTop: 22,
        paddingBottom: 22,
        paddingLeft: 30,
        paddingRight: 30,
    },
    headerText: {
        fontFamily: 'Helvetica',
        fontWeight: 'bold',
        color: '#fff',
        textAlign: 'center',
        fontSize: 18,
    },
});

export default styles;
