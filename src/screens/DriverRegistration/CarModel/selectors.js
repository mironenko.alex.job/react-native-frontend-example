/**
 * Car Model selectors
 */

import { createSelector } from 'reselect';
import { initialState } from './reducer';

const carModelState = (state) => state.user.model || initialState;

const selectCarModel = () => createSelector(carModelState, (pState) => pState.val);

// eslint-disable-next-line import/prefer-default-export
export { selectCarModel };
