import React, { useState } from 'react';
import {
    Text, View, TouchableOpacity, TextInput,
} from 'react-native';
import Autocomplete from 'react-native-autocomplete-input';

import PropTypes from 'prop-types';
import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';

import { setModel } from './actions';
import { selectCarModel } from './selectors';

import Page from 'components/Page';
import Header from 'components/Header';
import { buttonBuild } from 'utils/buttonsHelper';
import { navigate } from 'utils/navigationHelper';
import s from './styles';

const models = [
    {
        brand: 'Acura',
    },
    {
        brand: 'Alfa Romeo',
    },
    {
        brand: 'AMC',
    },
    {
        brand: 'Aston Martin',
    },
    {
        brand: 'Audi',
    },
    {
        brand: 'Avanti',
    },
    {
        brand: 'Bentley',
    },
    {
        brand: 'BMW',
    },
    {
        brand: 'Buick',
    },
    {
        brand: 'Cadillac',
    },
    {
        brand: 'Chevrolet',
    },
    {
        brand: 'Chrysler',
    },
    {
        brand: 'Daewoo',
    },
    {
        brand: 'Daihatsu',
    },
    {
        brand: 'Datsun',
    },
    {
        brand: 'DeLorean',
    },
    {
        brand: 'Dodge',
    },
    {
        brand: 'Eagle',
    },
    {
        brand: 'Ferrari',
    },
    {
        brand: 'FIAT',
    },
    {
        brand: 'Fisker',
    },
    {
        brand: 'Ford',
    },
    {
        brand: 'Freightliner',
    },
    {
        brand: 'Geo',
    },
    {
        brand: 'GMC',
    },
    {
        brand: 'Honda',
    },
    {
        brand: 'HUMMER',
    },
    {
        brand: 'Hyundai',
    },
    {
        brand: 'Infiniti',
    },
    {
        brand: 'Isuzu',
    },
    {
        brand: 'Jaguar',
    },
    {
        brand: 'Jeep',
    },
    {
        brand: 'Kia',
    },
    {
        brand: 'Lamborghini',
    },
    {
        brand: 'Lancia',
    },
    {
        brand: 'Land Rover',
    },
    {
        brand: 'Lexus',
    },
    {
        brand: 'Lincoln',
    },
    {
        brand: 'Lotus',
    },
    {
        brand: 'Maserati',
    },
    {
        brand: 'Maybach',
    },
    {
        brand: 'Mazda',
    },
    {
        brand: 'McLaren',
    },
    {
        brand: 'Mercedes-Benz',
    },
    {
        brand: 'Mercury',
    },
    {
        brand: 'Merkur',
    },
    {
        brand: 'MINI',
    },
    {
        brand: 'Mitsubishi',
    },
    {
        brand: 'Nissan',
    },
    {
        brand: 'Oldsmobile',
    },
    {
        brand: 'Peugeot',
    },
    {
        brand: 'Plymouth',
    },
    {
        brand: 'Pontiac',
    },
    {
        brand: 'Porsche',
    },
    {
        brand: 'RAM',
    },
    {
        brand: 'Renault',
    },
    {
        brand: 'Rolls-Royce',
    },
    {
        brand: 'Saab',
    },
    {
        brand: 'Saturn',
    },
    {
        brand: 'Scion',
    },
    {
        brand: 'smart',
    },
    {
        brand: 'SRT',
    },
    {
        brand: 'Sterling',
    },
    {
        brand: 'Subaru',
    },
    {
        brand: 'Suzuki',
    },
    {
        brand: 'Tesla',
    },
    {
        brand: 'Toyota',
    },
    {
        brand: 'Triumph',
    },
    {
        brand: 'Volkswagen',
    },
    {
        brand: 'Volvo',
    },
    {
        brand: 'Yugo',
    },
];
const comp = (a, b) => {
    if (a && b) {
        return a.toLowerCase().trim() === b.toLowerCase().trim();
    }
    return false;
};

const CarModel = (props) => {
    const { dispatch, carModel } = props;
    const [query, setQuery] = useState(carModel);

    const onQueryPlaces = () => {
        if (query === '') {
            return models;
        }

        const regex = new RegExp(`${query.trim()}`, 'i');
        return models.filter((model) => model.brand.search(regex) >= 0);
    };
    const targetColors = onQueryPlaces();
    const left = buttonBuild(
        () => navigate('driver-info'),
        'Назад',
        { color: '#FF8901' },
    );
    const center = <Text>Регистрация</Text>;
    const right = buttonBuild(
        () => navigate('dashboard'),
        'Закрыть',
        { color: '#A1A1AF' },
    );

    const onSetModel = (val) => {
        dispatch(setModel(val));
    };
    // console.log('Model', carModel);
    return (
        <Page bg="transparent">
            <Header left={left} center={center} right={right} />
            <View style={s.header}>
                <Text style={s.headerText}>Выберите марку машины на которой будите работать</Text>
            </View>
            <Autocomplete
                inputContainerStyle={{ borderWidth: 0, borderRadius: 30, backgroundColor: 'transparent' }}
                listContainerStyle={{
                    marginTop: 30, paddingLeft: 0, paddingRight: 0, width: '100%', marginLeft: 0, marginRight: 0,
                }}
                listStyle={{
                    width: '100%', paddingLeft: 0, paddingRight: 0, marginLeft: 0, marginRight: 0,
                }}
                autoCapitalize="none"
                autoCorrect={false}
                containerStyle={s.autocompleteContainer}
                data={targetColors.length === 1 && comp(query, targetColors[0].name)
                    ? []
                    : targetColors}
                defaultValue={query}
                onChangeText={(text) => setQuery(text)}
                placeholder="Выберите марку машины"
                renderItem={(model) => (
                    <TouchableOpacity
                        key={`car${model.item.brand}`}
                        style={[s.list]}
                        onPress={() => {
                            onSetModel(model.item.brand);
                            navigate('car-color');
                        }}
                    >
                        <Text style={s.listText}>{model.item.brand}</Text>
                    </TouchableOpacity>
                )}
                renderTextInput={(inputProps) => (
                    <TextInput
                        {...inputProps}
                        style={s.searchInput}
                    />
                )}
            />
        </Page>
    );
};
CarModel.propTypes = {
    dispatch: PropTypes.func,
    carModel: PropTypes.string,
};

const mapStateToProps = createStructuredSelector({
    carModel: selectCarModel(),
});

export default connect(mapStateToProps, null)(CarModel);
