import {
    SET_MODEL,
} from './constants';

/**
 * Set car model
 *
 * @param phone string
 */

export function setModel(model) {
    return {
        type: SET_MODEL,
        model,
    };
}