import React from 'react';
import {
    Text, View, Image, ScrollView,
} from 'react-native';

import Header from 'components/Header';
import Page from 'components/Page';
import { buttonBuild } from 'utils/buttonsHelper';
import TaxiButton from 'components/TaxiButton';
import { navigate } from 'utils/navigationHelper';

import List from 'img/list.png';
import Registration from 'img/registration.png';

import s from './styles';

const RegistrationRules = (props) => {
    // const { } = props;
    const left = buttonBuild(
        () => navigate('dashboard'),
        'Назад',
        { color: '#FF8901' },
    );
    const center = <Text>Регистрация</Text>;
    const right = buttonBuild(
        () => navigate('dashboard'),
        'Закрыть',
        { color: '#A1A1AF' },
    );
    const data = [
        { key: 'Ваша фотография' },
        { key: 'Фотография машины' },
        { key: 'Свидетельство регистрации ТС' },
        { key: 'Водительское удостоверение' },
    ];
    const list = data.map((item, i) => (
        <View key={`list${i}`} style={[s.list]}>
            <Image source={List} />
            <Text style={[s.listText]}>{item.key}</Text>
        </View>
    ));
    return (
        <Page bg="transparent">
            <Header left={left} center={center} right={right} />
            <ScrollView>
                <View style={[s.wrap, s.whiteWrap]}>
                    <View style={[s.imgBlock]}>
                        <Image source={Registration} />
                    </View>

                    <Text style={[s.titleText]}>
                        Для получения заказов пройдите online-регистрацию водителя
                    </Text>
                </View>
                <View style={[s.wrap, s.greyWrap]}>
                    <Text style={[s.listTitle]}>Вам понадобиться</Text>
                    {list}
                </View>
                <View style={[s.buttonWrap]}>
                    <TaxiButton
                        title="Пройти регистрацию"
                        elColor="#FECD0E"
                        handler={() => navigate('photo-rules')}
                    />
                </View>
            </ScrollView>
        </Page>
    );
};
export default RegistrationRules;
