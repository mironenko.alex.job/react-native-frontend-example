import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    wrap: {
        width: '100%',
        display: 'flex',
        flexDirection: 'column',
        borderBottomColor: '#E2E2EC',
        borderBottomWidth: 1,
    },
    whiteWrap: {
        alignItems: 'center',
        paddingTop: 32,
        paddingBottom: 28,
        backgroundColor: '#fff',
    },
    greyWrap: {
        paddingTop: 33,
        paddingBottom: 28,
        paddingRight: 26,
        paddingLeft: 26,
        backgroundColor: '#F1F2F6',
    },
    imgBlock: {
        width: 173,
        height: 173,
        borderRadius: 173,
        backgroundColor: '#F4F3F0',
        marginBottom: 32,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
    },
    titleText: {
        fontFamily: 'Segoeui',
        textAlign: 'center',
        fontSize: 20,
        lineHeight: 27,
        color: '#282C38',
        textShadowColor: 'rgba(0, 0, 0, 0.25)',
        textShadowOffset: { width: 0, height: 4 },
        textShadowRadius: 4,
    },
    list: {
        display: 'flex',
        alignItems: 'center',
        flexDirection: 'row',
        marginBottom: 15,
    },
    listTitle: {
        fontFamily: 'Segoeui',
        fontSize: 18,
        lineHeight: 24,
        color: '#282C38',
        fontWeight: 'bold',
        marginBottom: 30,
    },
    listText: {
        fontFamily: 'Segoeui',
        fontSize: 15,
        lineHeight: 20,
        color: '#242A38',
        marginLeft: 10,
    },
    buttonWrap: {
        marginTop: 48,
        paddingRight: 25,
        paddingLeft: 25,
        marginBottom: 40,
    },
});

export default styles;
