/**
 * Car Color selectors
 */

import { createSelector } from 'reselect';
import { initialState } from './reducer';

const carColorState = (state) => state.user.color || initialState;

const selectCarColor = () => createSelector(carColorState, (pState) => pState.val);

// eslint-disable-next-line import/prefer-default-export
export { selectCarColor };
