import {
    SET_COLOR,
} from './constants';

/**
 * Set car color
 *
 * @param color object
 */

export function setColor(color) {
    return {
        type: SET_COLOR,
        color,
    };
}