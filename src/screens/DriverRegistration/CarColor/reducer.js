import produce from 'immer';
import { SET_COLOR } from './constants';
// Initial State
export const initialState = {
    val: {
        hex: '',
        name: '',
    },
};

const carColorReducer = (state = initialState, action) => produce(state, (draft) => {
    switch (action.type) {
    case SET_COLOR:
        draft.val = action.color;
        break;
    default: {
        return state;
    }
    }
});

export default carColorReducer;
