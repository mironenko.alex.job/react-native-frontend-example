import {
    SET_LICENSE,
} from './constants';

/**
 * Set car license
 *
 * @param license string
 */

export function setLicense(license) {
    return {
        type: SET_LICENSE,
        license,
    };
}