/**
 * Car Color selectors
 */

import { createSelector } from 'reselect';
import { initialState } from './reducer';

const carLicenseState = (state) => state.user.license || initialState;

const selectCarLicense = () => createSelector(carLicenseState, (pState) => pState.val);

// eslint-disable-next-line import/prefer-default-export
export { selectCarLicense };
