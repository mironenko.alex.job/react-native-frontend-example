import React from 'react';
import { Text, View } from 'react-native';
import { Input } from 'react-native-elements';
import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';
import Page from 'components/Page';
import Header from 'components/Header';
import { buttonBuild } from 'utils/buttonsHelper';
import TaxiButton from 'components/TaxiButton';
import { navigate } from 'utils/navigationHelper';
import s from 'src/screens/DriverRegistration/CarLicense/styles';
import PropTypes from 'prop-types';
import { selectCarModel } from '../CarModel/selectors';
import { selectCarColor } from '../CarColor/selectors';
import { selectCarLicense } from './selectors';
import { setLicense } from './actions';

const CarLicense = (props) => {
    const {
        carColor, carModel, carLicense, dispatch,
    } = props;
    const left = buttonBuild(
        () => navigate('car-color'),
        'Назад',
        { color: '#FF8901' },
    );
    const center = <Text>Госномер</Text>;
    const right = buttonBuild(
        () => navigate('dashboard'),
        'Закрыть',
        { color: '#A1A1AF' },
    );
    const onSetLicense = (val) => {
        dispatch(setLicense(val));
    };

    console.log('Car license', carLicense);
    return (
        <Page bg="transparent">
            <Header left={left} center={center} right={right} />
            <View style={s.carModelWrap}>
                <Text style={s.carModel}>{carModel}</Text>
                <Text style={[s.carColor, { color: carColor.hex }]}>{carColor.name}</Text>
            </View>
            <View style={s.carNumberWrap}>
                <Text style={s.carNumberLabel}>Ввeдите госномер вашего авто</Text>
                <Input
                    inputContainerStyle={{ borderBottomColor: '#E0E0E7' }}
                    inputStyle={s.carNumber}
                    placeholder="A145ВК"
                    value={carLicense}
                    onChangeText={(text) => onSetLicense(text)}
                    containerStyle={{ paddingLeft: 0 }}
                />
                <Text style={s.underCarNumberText}>
                    Ваш номер нужен только для того что бы пассажир смог найти ваше авто
                </Text>
            </View>
            <View style={[s.buttonWrap]}>
                <TaxiButton
                    title="ДАЛЕЕ"
                    elColor="#FECD0E"
                    handler={() => navigate('car-license-additional')}
                />
            </View>
        </Page>
    );
};
CarLicense.propTypes = {
    dispatch: PropTypes.func,
    carColor: PropTypes.object,
    carModel: PropTypes.string,
    carLicense: PropTypes.string,
};

const mapStateToProps = createStructuredSelector({
    carColor: selectCarColor(),
    carModel: selectCarModel(),
    carLicense: selectCarLicense(),
});

export default connect(mapStateToProps, null)(CarLicense);
