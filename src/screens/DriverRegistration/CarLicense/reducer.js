import produce from 'immer';
import { SET_LICENSE } from './constants';
// Initial State
export const initialState = {
    val: 'Test',
};

const carLicenseReducer = (state = initialState, action) => produce(state, (draft) => {
    switch (action.type) {
    case SET_LICENSE:
        draft.val = action.license;
        break;
    default: {
        return state;
    }
    }
});

export default carLicenseReducer;
