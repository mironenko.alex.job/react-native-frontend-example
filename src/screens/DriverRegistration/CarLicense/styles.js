import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    carModelWrap: {
        borderBottomColor: '#E0E0E7',
        borderBottomWidth: 1,
        backgroundColor: '#fff',
        paddingTop: 30,
        paddingBottom: 30,
    },
    carModel: {
        fontFamily: 'SegoeuiBold',
        fontSize: 20,
        lineHeight: 27,
        textAlign: 'center',
        paddingBottom: 10,
    },
    carColor: {
        color: '#FECD0E',
        textAlign: 'center',
    },
    carNumberWrap: {
        borderBottomColor: '#E0E0E7',
        borderBottomWidth: 1,
        backgroundColor: '#fff',
        paddingTop: 30,
        paddingBottom: 30,
        paddingLeft: 42,
        paddingRight: 42,
    },
    carNumberLabel: {
        fontFamily: 'Segoeui',
        fontSize: 14,
        color: '#282C38',
        textAlign: 'left',
    },
    carNumber: {
        color: '#282C38',
    },
    underCarNumberText: {
        fontFamily: 'Segoeui',
        color: '#282C38',
        textAlign: 'left',
        fontSize: 10,
    },
    buttonWrap: {
        marginTop: 48,
        paddingRight: 25,
        paddingLeft: 25,
    },
});

export default styles;
