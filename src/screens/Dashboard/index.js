import React, { useState, useEffect } from 'react';
import SwipePanel from 'containers/swipePanel';
import Geocoder from 'react-native-geocoding';
import Geolocation from 'react-native-geolocation-service';

import PropTypes from 'prop-types';
import {
    Text, View, Dimensions, PermissionsAndroid, SafeAreaView,
} from 'react-native';
import MapView from 'react-native-maps';

import DrawerBtn from 'containers/Dashboard/DrawerBtn';
import ShareBtn from 'containers/Dashboard/ShareBtn';
import Page from 'components/Page';
import s from './styles';

const Dashboard = (props) => {
    const { navigation } = props;
    const [latitude, setLatitude] = useState();
    const [longitude, setLongitude] = useState();

    // useEffect(() => {
    //     Geolocation.getCurrentPosition((position) => {
    //         setLatitude(position.coords.latitude);
    //         setLongitude(position.coords.longitude);
    //     });
    // });

    return (
        <Page>
            <View style={s.header}>
                <DrawerBtn navigation={navigation} />
                <Text style={s.headerText}>Город</Text>
                <ShareBtn />
            </View>


            <SwipePanel>
                <View style={s.map}>
                    <MapView
                        followsUserLocation
                        showsUserLocation
                        showsMyLocationButton
                        showsCompass
                        toolbarEnabled
                        zoomEnabled
                        rotateEnabled
                        style={{
                            width: Dimensions.get('window').width,
                            height: Dimensions.get('window').height,
                        }}
                        minZoomLevel={15}
                        region={{
                            latitude: 49.9608227,
                            longitude: 36.339723,
                            latitudeDelta: 0.01,
                            longitudeDelta: 0.01,
                        }}
                        onMapReady={() => {
                            PermissionsAndroid.request(
                                PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
                            ).then();
                        }}
                    />
                </View>

            </SwipePanel>
        </Page>
    );
};
export default Dashboard;
