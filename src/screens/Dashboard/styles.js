import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    container: {
        //flex: 1,
        backgroundColor: '#fff',
    },
    header: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        backgroundColor: 'transparent',
        paddingTop: 67,
        paddingBottom: 22,
        position: 'absolute',
        top: 0,
        right: 0,
        zIndex: 9999,
        elevation: 9999,
        width: '100%',
    },
    headerText: {
        fontFamily: 'SegoeuiBold',
        fontWeight: 'bold',
        color: '#282C38',
        textAlign: 'center',
        fontSize: 16,
    },
    map: {
        elevation: -1,
        //position: 'absolute',
    },
});
export default styles;
