import React from 'react';

import WelcomeInfoGallery from 'containers/WelcomeInfoGallery';
import PriceImg from 'img/WelcomeInfo/price.png';
import AcceptImg from 'img/WelcomeInfo/accept.png';
import CarImg from 'img/WelcomeInfo/car.png';

const WelcomeInfo = () => {
    const imagesList = [
        {
            url: PriceImg,
            title: 'Предлагай свою цену за поездку',
            text: 'Укажите свой маршрут и цену, мы предложим ваш заказ водителям',
        },
        {
            url: AcceptImg,
            title: 'Быстрая подача машины',
            text: 'Заказ отправляется ближайшим водителям',
        },
        {
            url: CarImg,
            title: 'Есть авто? Зарабатывай с нами!',
            text: 'Активируй в меню “Режим водителя” и выбирай заказы',
        },
    ];
    return <WelcomeInfoGallery images={imagesList} />;
};

export default WelcomeInfo;
