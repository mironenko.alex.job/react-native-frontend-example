module.exports = function (api) {
    api.cache(true);
    return {
        presets: ['babel-preset-expo'],
        plugins: [
            [
                'module-resolver',
                {
                    root: ['./src'],
                    extensions: ['.ios.js', '.android.js', '.js', '.ts', '.tsx', '.json'],
                    alias: {
                        src: './src',
                        assets: './assets',
                        img: './src/img',
                        components: './src/components',
                        containers: './src/containers',
                        fonts: './src/fonts',
                        screens: './src/screens',
                        utils: './src/utils',
                        global: './src/global',
                    },
                },
            ],
            [
                'babel-plugin-inline-import',
                {
                    extensions: ['.svg'],
                },
            ],
        ],
    };
};
